import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import Login from "./Login";
import userApi from "../../api/userApi";
import { ValueLogin } from "../../Interface";
import { useHistory } from "react-router-dom";

const LoginPage: React.FC = () => {
  let history = useHistory();
  const token: string = JSON.parse(localStorage.getItem("token") || "{}");
  const [disableButton, setDisableButton] = useState<boolean>(false);

  useEffect(() => {
    if (token.length) {
      history.push("/");
    }
  });

  function handleSubmit(params: ValueLogin) {
    setDisableButton(true);
    if (params) {
      const loginUser = async () => {
        await userApi
          .login(params)
          .then((res) => {
            if (res.data === null) {
              throw res;
            }
            const { data } = res;
            localStorage.setItem("user", JSON.stringify(data.user));
            localStorage.setItem(
              "token",
              JSON.stringify("Bearer " + data.token)
            );
            toast.success("Đăng nhập thành công!");
            setDisableButton(false);
            history.push("/");
          })
          .catch((err) => {
            toast.error(err.msg || "Đăng nhập thất bại!");
            setDisableButton(false);
          });
      };
      loginUser();
    }
  }
  return <Login onSubmit={handleSubmit} disableButton={disableButton} />;
};

export default LoginPage;

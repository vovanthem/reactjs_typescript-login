import React, { useState } from "react";

import { ValueLogin } from "../../Interface";

interface Props {
  onSubmit: (param: ValueLogin) => void;
  disableButton: boolean;
}

const Login: React.FC<Props> = ({ onSubmit, disableButton }) => {
  const [user, setUser] = useState<ValueLogin>({
    email: "test@ibenefit.vn",
    password: "abc123456",
  });

  const [noteMail, setNoteMail] = useState("");
  const [notePass, setNotePass] = useState("");
  const regExp: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  function onSubmitForm(e: any) {
    e.preventDefault();
  }

  function handleClick() {
    if (!user.email || !user.password) {
      alert("Vui lòng nhập đủ thông tin đăng nhập.");
      return;
    }
    let valueUser = {
      email: user.email,
      password: user.password,
    };
    onSubmit(valueUser);
  }

  function handleEmailOnBlur() {
    if (!regExp.test(String(user.email).toLowerCase())) {
      setNoteMail("Vui lòng nhập đúng địa chỉ email");
    } else {
      setNoteMail("");
    }
  }
  function handlePassOnBlur() {
    if (user.password.length < 8) {
      setNotePass("Vui lòng nhập pass ít nhất 8 ký tự");
    } else {
      setNotePass("");
    }
  }


  return (
    <div className="Login">
      <form onSubmit={onSubmitForm} className="formLogin">
        <div className="header">
          <p>ĐĂNG NHẬP</p>
        </div>
        <div className="form-group">
          <label>Email address</label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            placeholder="Enter email"
            value={user?.email}
            onChange={(e) => setUser({ ...user, email: e.target?.value })}
            onBlur={handleEmailOnBlur}
            style={{ border: !noteMail ? "" : "1px solid red" }}
          />
          <small id="emailHelp" className="form-text text-muted">
            {noteMail}
          </small>
        </div>
        <div className="form-group">
          <label>Password</label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
            placeholder="Password"
            value={user?.password}
            onChange={(e) => setUser({ ...user, password: e.target?.value })}
            onBlur={handlePassOnBlur}
            style={{ border: !notePass ? "" : "1px solid red" }}
          />
          <small id="emailHelp" className="form-text text-muted">
            {notePass}
          </small>
        </div>
        <button
          type="submit"
          disabled={disableButton}
          onClick={handleClick}
          className="btn btn-primary btnLogin"
        >
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;

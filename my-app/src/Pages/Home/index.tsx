import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

function Home() {
  const token: string = JSON.parse(localStorage.getItem("token") || "{}");
  const userLogin: any = JSON.parse(localStorage.getItem("user") || "{}");
  let history = useHistory();

  useEffect(() => {
    if (!token.length) {
      history.push("/login");
      return;
    }
    history.push("/");
  });

  function handleLogout() {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    toast.warning("Đăng xuất thành công!");
    history.push("/login");
  }
  return (
    <div>
      <h1>Day la trang home{userLogin?.id}</h1>
      <button onClick={handleLogout}>Logout</button>
    </div>
  );
}

export default Home;

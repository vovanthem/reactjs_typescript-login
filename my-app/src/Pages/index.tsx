import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Home";
import LoginPage from "./Login";

export default function MainRoute() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/login">
          <LoginPage />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

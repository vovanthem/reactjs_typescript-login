import axiosClient from './axiosClient';

 const getDeviceCodeApi = {
    deviceCode(){
        const url = '/api/device/init';
        const deviceType = { device_type: 2 };
        return axiosClient.post(url, deviceType)
    }
}

export default getDeviceCodeApi;
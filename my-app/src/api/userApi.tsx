import axiosClient from './axiosClient';
import {ValueLogin} from '../Interface';

const userApi = {
    login(param:ValueLogin){
        const url='/api/auth/login'
        return axiosClient.post(url,param);
    }
}

export default userApi;
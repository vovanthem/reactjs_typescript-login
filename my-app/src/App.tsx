import React, { useState, useEffect } from "react";
import "./App.css";
import getDeviceCodeApi from "./api/getDeviceCodeApi";
import MainRoute from "./Pages";
import Loading from "./components/icon/Loading";
const DeviceCode: string = JSON.parse(
  localStorage.getItem("DeviceCode") || "{}"
);

const App: React.FC = () => {
  const [deviceCode, setDeviceCode] = useState<string>(DeviceCode);

  useEffect(() => {
    if (!deviceCode.length) {
      const getDeviceCode = async () => {
        const device = await getDeviceCodeApi.deviceCode();
        if (!device) {
          throw device;
        }
        const { data } = device;
        localStorage.setItem("DeviceCode", JSON.stringify(data?.device_code));
        setDeviceCode(data?.device_code);
      };
      getDeviceCode();
    }
  });

  return (
    <div className="App">
      {!!deviceCode.length ? <MainRoute /> : <Loading />}
    </div>
  );
};

export default App;

import styled, { keyframes } from 'styled-components';
type Custom = {
  loading: number
}
const rotate = keyframes`
from {
  transform: rotate(0deg);
}

to {
  transform: rotate(360deg);
}
`;

export const IconContainer = styled.span<Custom>`
  width: var(--icon-size-small);
  height: var(--icon-size-small);
  display: inline-block;
  animation: ${({ loading }) => loading && rotate} 1s linear infinite;
  color: var(--primary-color);
  margin-top: var(--space-0);
  svg {
    width: 100%;
    height: 100%;
  }
`;


export const StyleDivLoading = styled.div`
  display: flex;
  position: absolute;
  width: 100%;
  top: 0;
  bottom: 0;
  justify-content: center;
  align-items: center;
  border-radius: var(--space-3);
`;

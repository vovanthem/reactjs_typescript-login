import React from "react";
import { IconContainer, StyleDivLoading } from "./style";
import { ReactComponent as LoadingSvgIcon } from "../../assets/loading.svg";

function Loading() {
  return (
    <StyleDivLoading>
      <IconContainer loading={1}>
        <LoadingSvgIcon />
      </IconContainer>
    </StyleDivLoading>
  );
}

export default Loading;
